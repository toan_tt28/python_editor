import React from 'react'

import Provider from 'src/shared/Provider'
import { BrowserRouter } from 'react-router-dom'
import { observer } from 'mobx-react'
import { ConfigProvider } from 'antd'
import { Helmet } from 'react-helmet'

import Notification from 'src/shared/Notification'
import 'src/styles/antd/App.less'
import './App.less'
import AppRoutes from './shared/AppRoutes'

const SharedComponents = observer(() => {
    return (
        <>
            <Notification />
        </>
    )
})

const App: React.FC = () => {
    return (
        <Provider>
            <ConfigProvider>
                <BrowserRouter>
                    <Helmet>
                        <title>Python Editor</title>
                        <meta property="og:title" content="Simple Python Editor" />
                        <meta property="og:description" content="Run code python" />
                    </Helmet>
                    <AppRoutes />
                    <SharedComponents />
                </BrowserRouter>
            </ConfigProvider>
        </Provider>
    )
}

export default App
