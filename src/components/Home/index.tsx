import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Controlled as CodeMirror } from 'react-codemirror2'
import { Button } from 'antd'
import * as codemirror from 'codemirror'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import useRootStore from 'src/store/useRootStore'

require('codemirror/mode/python/python')

interface Props {
    value: string
    setValue: (x: string) => void
    editor1: codemirror.Editor | null
    setEditor1: (x: codemirror.Editor) => void
    editor2: codemirror.Editor | null
    setEditor2: (x: codemirror.Editor) => void
}

const Component: React.FC<Props> = (props) => {
    const { globalStore } = useRootStore()
    const [disabled, setDisabled] = useState<boolean>(true)
    const [initialValue, setInitialValue] = useState<string>('')

    useEffect(() => {
        setInitialValue(props.value)
    }, [])

    const highlightLines = (editor: codemirror.Editor, start: number, end: number, origin: string) => {
        const from = { line: start, ch: 0 }
        const to = { line: end, ch: 10000 }
        editor.markText(from, to, { className: origin === '+input' ? 'line-add' : 'line-remove' })
    }

    return (
        <Wrapper>
            <div className="mb-5">
                <CopyToClipboard
                    text={props.value}
                    onCopy={() => globalStore.successNotification({ message: 'Copied' })}
                >
                    <Button disabled={disabled || props.value.length === 0}>Copy</Button>
                </CopyToClipboard>
            </div>

            <div className="mb-5">
                <CodeMirror
                    value={props.value}
                    options={{
                        lineNumbers: true,
                        mode: 'python',
                    }}
                    onBeforeChange={(editor, data, value) => {
                        props.setValue(value)
                    }}
                    onChange={(editor, data, value) => {
                        if (props.editor2) {
                            const origin = data.origin || ''
                            highlightLines(props.editor2, data.from.line, data.from.line, origin)
                        }

                        setDisabled(value === initialValue)
                    }}
                />
            </div>

            <div className="mb-5">
                <CodeMirror
                    value={props.value}
                    options={{
                        lineNumbers: true,
                        readOnly: true,
                        mode: 'python',
                    }}
                    onBeforeChange={() => {
                        console.log('panel2')
                    }}
                    editorDidMount={(editor) => {
                        props.setEditor2(editor)
                    }}
                />
            </div>
        </Wrapper>
    )
}

export default Component

const Wrapper = styled('div')`
    .mb-5 {
        margin-bottom: 5px;
    }

    .line-add {
        background-color: greenyellow;
    }

    .line-remove {
        background-color: indianred;
    }
`
