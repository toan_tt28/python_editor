import React from 'react'
import { Layout } from 'antd'
import styled from 'styled-components'

// import history from 'src/utils/history'
import { routerStore } from 'src/store'
import { HEADER_HEIGHT } from 'src/constants/app'

const { Header } = Layout

type Props = {
    isCollapsed?: boolean
}

const HeaderNav: React.FC<Props> = () => {
    const history = routerStore.history

    return (
        <StyleHeader>
            <Logo onClick={() => history.push('/')}>PYTHON EDITOR</Logo>
        </StyleHeader>
    )
}

const StyleHeader = styled(Header)`
    position: fixed;
    padding: 0 1rem;
    z-index: 1;
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    line-height: ${HEADER_HEIGHT}px;
    height: ${HEADER_HEIGHT}px;
`

const Logo = styled.div`
    color: white;
    cursor: pointer;
`

export default HeaderNav
