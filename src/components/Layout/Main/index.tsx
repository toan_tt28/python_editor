import React from 'react'
import { Layout as AntdLayout, Grid } from 'antd'
import styled from 'styled-components'

import { HEADER_HEIGHT } from 'src/constants/app'
import HeaderNav from './HeaderNav'

const { Content } = AntdLayout

interface LayoutProps {
    children: React.ReactNode
}

const Layout = (props: LayoutProps) => {
    const { children } = props

    const screen = Grid.useBreakpoint()

    return (
        <AntdLayout>
            <AntdLayout id="inner-layout">
                <HeaderNav isCollapsed={!screen.sm} />
                <ContentWrapper style={{ paddingTop: `${HEADER_HEIGHT * 2}px`, paddingLeft: 20, paddingRight: 20 }}>
                    {children}
                </ContentWrapper>
            </AntdLayout>
        </AntdLayout>
    )
}

export default Layout

const ContentWrapper = styled(Content)`
    min-height: 100vh;
`
