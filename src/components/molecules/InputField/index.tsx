import * as React from 'react'
import { FieldProps } from 'formik'
import { Input, Form } from 'antd'
import styled from 'styled-components'

interface InputFieldProps extends FieldProps {
    label?: string
    required?: boolean
    submitCount: number
    placeholder?: string
    prefix?: string | React.ReactNode
    type?: string
    allowClear?: boolean
    maxlength?: number
    style?: any
    suffix?: any
}

const InputField = (props: InputFieldProps) => {
    const {
        label,
        form,
        field,
        submitCount,
        required,
        placeholder,
        prefix,
        type,
        maxlength,
        allowClear,
        style,
        suffix,
    } = props

    const { touched, errors }: any = form

    const isTouched = touched[field.name]
    const submitted = submitCount > 0
    const hasError = errors[field.name]
    const submittedError = hasError && submitted
    const touchedError = hasError && isTouched

    return (
        <Styled style={style}>
            <Form.Item
                label={label}
                help={submittedError || touchedError ? hasError : false}
                validateStatus={(submittedError || touchedError) && 'error'}
                required={required}
                colon={false}
            >
                <Input
                    {...field}
                    suffix={suffix}
                    placeholder={placeholder}
                    prefix={prefix}
                    maxLength={maxlength}
                    type={type}
                    allowClear={allowClear}
                />
            </Form.Item>
        </Styled>
    )
}

export default InputField

const Styled = styled.div`
    width: 100%;
    .ant-form-item-label {
        text-align: left;
    }
    .ant-form-item-with-help {
        margin-bottom: 12px;
    }
    .ant-input {
        padding: 10px 16px;
        font-size: 14px;
        border: none;
        border-radius: 0;
    }
    .ant-form-item-explain {
        margin-top: 4px;
        font-weight: 500;
        font-size: 14px;
    }
`
