// Antd breakpoints - https://ant.design/components/grid/#Col

const breakpoint = {
    sm: '576px' as const,
    md: '768px' as const,
    lg: '992px' as const,
    xl: '1200px' as const,
    xxl: '1600px' as const,
}

export default breakpoint
