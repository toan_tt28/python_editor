// ENVIRONMENT
export const TTUTA_API_URL = process.env.REACT_APP_TTUTA_API_URL || ''
export const WALLET_API_URL = process.env.REACT_APP_WALLET_API_URL || ''
export const APP_NAME = process.env.REACT_APP_APP_NAME || 'admin_wallet'

// KEY NAME
export const ACCESS_TOKEN_KEY = `${APP_NAME}_access_token`
export const NAV_OPEN_KEYS = 'nav_open_key'
export const SIDE_BAR_THEME = 'side_bar_theme'
export const SIDE_BAR_COLLAPSED = 'side_bar_collapsed'
