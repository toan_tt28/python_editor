import { observer } from 'mobx-react'
import React from 'react'

import LayoutMain from 'src/components/Layout/Main'

interface ContainerComponentProps {
    children: React.ReactNode
}

const ContainerComponent = (props: ContainerComponentProps) => {
    const containerProps = {
        ...props,
    }

    return <LayoutMain {...containerProps} />
}

export default observer(ContainerComponent)
