import React, { useState } from 'react'
import Home from 'src/components/Home'
import { observer } from 'mobx-react'
import * as codemirror from 'codemirror'

const ContainerComponent = () => {
    const [value, setValue] = useState<string>(`
import banana


class Monkey:
    # Bananas the monkey can eat.
    capacity = 10
    def eat(self, n):
        """Make the monkey eat n bananas!"""
        self.capacity -= n * banana.size

    def feeding_frenzy(self):
        self.eat(9.25)
        return "Yum yum"
    `)

    const [editor1, setEditor1] = useState<codemirror.Editor | null>(null)
    const [editor2, setEditor2] = useState<codemirror.Editor | null>(null)
    const containerProps = {
        value,
        setValue,
        editor1,
        setEditor1,
        editor2,
        setEditor2,
    }

    return <Home {...containerProps} />
}

export default observer(ContainerComponent)
