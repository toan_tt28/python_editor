export type PaginationData = {
    current: number
    defaultCurrent: number
    total: number
    pageSize: number
    defaultPageSize: number
}

export type Pagination = {
    count: number
    items: number
    page: number
    last: number
    pages: number
    offset: number
    from: number
    to: number
}

export type Wallet = {
    id: number
    chain_network_id: number
    address: string
    address_path: string
    cold_address: string | null
    min_collect_amount: number
    wallet_type: string
    created_at: string
    address_link: string
}

export type User = {
    id: number
    user_uid: string
    status: string
    has_pincode: boolean
    pincode_retried_times: number
    locked_expired_at: string | null
    verify_expired_at: string | null
    address: string
}

export type Account = {
    balance: number
    locked: number
    deposit_address: string
    asset: string
    asset_name: string
    asset_icon: string
    chain: string
    is_locked: boolean
}

export type TokenDistribution = {
    id: string
    user_uid: string
    amount: number
    unlock_at: string | null
    asset: string
    status: string | null
}

export type Transaction = {
    id: string
    transaction_type: string
    title: string
    is_debit: boolean
    debtor: string
    debtor_name: string | null
    creditor: string
    creditor_name: string | null
    asset: string
    amount: number
    fee: number
    txid: string
    tx_url: string | null
    is_blockchain: boolean
    description: string
    status: string
    created_at: string
}

export enum TransactionType {
    Deposit,
    Withdrawal,
    PointExchange,
    Swap,
    Reward,
    Promotion,
    Refund,
    Pay,
    Distribution,
    SafePayment,
    RewardCancel,
    BuyFtsy,
}
