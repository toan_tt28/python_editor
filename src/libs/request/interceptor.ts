import axios from 'axios'
import NProgress from 'nprogress'

import { routerStore } from 'src/store'
import HttpStatusCode from 'src/constants/http-status-code'
import { getAccessToken } from 'src/utils/localstorage'
import { logout } from 'src/usecases/authorization'

NProgress.configure({ showSpinner: false })

axios.interceptors.request.use(
    (config) => {
        NProgress.start()
        const accessToken = getAccessToken()

        if (accessToken) {
            const headers = {
                Authorization: `Bearer ${accessToken}`,
            }

            config.headers = Object.assign(config.headers, headers)
        }

        return config
    },
    (error) => Promise.reject(error),
)

axios.interceptors.response.use(
    (response) => {
        NProgress.done()

        return response
    },
    (error) => {
        NProgress.done()
        const { push } = routerStore

        const statusCode = error.response && error.response.status
        switch (statusCode) {
            case HttpStatusCode.UNAUTHORIZED:
                logout()
                break
            case HttpStatusCode.FORBIDDEN:
                push('/403')
                break
            case HttpStatusCode.NOT_FOUND:
                // push('/404')
                break
            case HttpStatusCode.INTERNAL_SERVER_ERROR:
                // push('/500')
                break
            default:
                break
        }

        NProgress.done()
        return Promise.reject(error)
    },
)
