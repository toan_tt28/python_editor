import React from 'react'
import HomeContainer from 'src/containers/pages/Home'
import LayoutMain from 'src/containers/Layout/Main'

const Page = () => {
    return (
        <LayoutMain>
            <HomeContainer />
        </LayoutMain>
    )
}

export default Page
