import { AxiosResponse } from 'axios'
import HttpRequest from 'src/libs/request'

type LoginResponse = {
    code: number
    data: {
        id: number
        token: string
    }
}

type LoginBody = {
    phone: string
    pwd: string
}

class TtutaRepository extends HttpRequest {
    login<T = LoginResponse>(body: LoginBody): Promise<AxiosResponse<T>> {
        return this.post(`/dutta/api/login`, body)
    }
}

export default new TtutaRepository()
