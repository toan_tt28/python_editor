import { AxiosResponse } from 'axios'
import HttpRequest from 'src/libs/request'
import { Account, Pagination, TokenDistribution, Transaction, User } from 'src/interfaces'
import { WALLET_API_URL } from 'src/config'

type FetchResponse = {
    status: number
    code: number
    data: User[]
    meta: {
        pagination: Pagination
    }
}

type FetchAccountsResponse = {
    status: number
    code: number
    data: Account[]
}

type FetchTokenDistributionsResponse = {
    status: number
    code: number
    data: TokenDistribution[]
}

type FetchTransactionsResponse = {
    status: number
    code: number
    data: Transaction[]
    meta: {
        pagination: Pagination
    }
}

type PaginateParams = {
    page: number
    per: number
}

class UserRepository extends HttpRequest {
    constructor() {
        super()
        this.apiUrl = WALLET_API_URL
    }

    fetch<T = FetchResponse>(params: PaginateParams): Promise<AxiosResponse<T>> {
        return this.get(`/admin/users`, params)
    }

    fetchAccounts<T = FetchAccountsResponse>(userId: string): Promise<AxiosResponse<T>> {
        return this.get(`/admin/users/${userId}/accounts`)
    }

    fetchTokenDistributions<T = FetchTokenDistributionsResponse>(userId: string): Promise<AxiosResponse<T>> {
        return this.get(`/admin/users/${userId}/token-distributions`)
    }

    fetchTransactions<T = FetchTransactionsResponse>(
        userId: string,
        params: PaginateParams,
    ): Promise<AxiosResponse<T>> {
        return this.get(`/admin/users/${userId}/transactions`, params)
    }
}

export default new UserRepository()
