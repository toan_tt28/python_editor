import { AxiosResponse } from 'axios'
import HttpRequest from 'src/libs/request'
import { Wallet } from 'src/interfaces'
import { WALLET_API_URL } from 'src/config'

type FetchResponse = {
    status: number
    code: number
    data: Wallet[]
}

class WalletRepository extends HttpRequest {
    constructor() {
        super()
        this.apiUrl = WALLET_API_URL
    }

    fetch<T = FetchResponse>(): Promise<AxiosResponse<T>> {
        return this.get(`/admin/hot_wallets`)
    }
}

export default new WalletRepository()
