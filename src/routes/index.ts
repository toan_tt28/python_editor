import { RouteProps } from 'react-router'
// import { NotFoundPage, ForbiddenPage, ServerErrorPage } from 'src/app/pages'

export interface IRouter extends RouteProps {
    requiredAuth?: boolean
    restricted?: boolean
    redirect?: boolean
    redirectWithEffect?: boolean
    to?: string
    effect?: (x: any) => void
}

const modulesRoute = () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const requireModule = require.context('./modules', true, /\.ts$/)
    const modules: IRouter[] = []

    requireModule.keys().forEach((fileName: string) => {
        const module: IRouter[] = requireModule(fileName).default
        modules.push(...module)
    })

    return modules
}

const routes: IRouter[] = [
    ...modulesRoute(),
    // {
    //     path: '/403',
    //     exact: true,
    //     component: ForbiddenPage,
    // },
    // {
    //     path: '/404',
    //     exact: true,
    //     component: NotFoundPage,
    // },
    // {
    //     path: '/500',
    //     exact: true,
    //     component: ServerErrorPage,
    // },
    // {
    //     path: '*',
    //     component: NotFoundPage,
    // },
]
export default routes
