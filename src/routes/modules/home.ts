import Home from 'src/pages/Home'
import { IRouter } from 'src/routes'

const routes: IRouter[] = [
    {
        path: '/',
        component: Home,
        exact: true,
        strict: true,
        restricted: true,
        requiredAuth: false,
    },
]

export default routes
