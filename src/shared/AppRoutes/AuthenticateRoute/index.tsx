import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import useRootStore from 'src/store/useRootStore'
import { observer } from 'mobx-react'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const AuthenticatedRoute = ({ component: Component, ...rest }) => {
    const { globalStore } = useRootStore()

    if (globalStore.isAuthenticated) {
        return <Route {...rest} render={(props) => <Component {...props} key={globalStore.forceReload} />} />
    }

    return (
        <Redirect
            to={{
                pathname: '/login',
            }}
        />
    )
}

export default observer(AuthenticatedRoute)
