import React from 'react'
import { Route } from 'react-router-dom'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const ErrorRoute = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(props) => <Component {...props} />} />
}

export default React.memo(ErrorRoute)
