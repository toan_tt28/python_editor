import React from 'react'
import { Redirect } from 'react-router-dom'

const RedirectRoute = ({ redirectPath }: { redirectPath: string }) => {
    return (
        <Redirect
            to={{
                pathname: redirectPath,
            }}
        />
    )
}

export default React.memo(RedirectRoute)
