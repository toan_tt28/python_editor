import React, { useEffect } from 'react'
import { Redirect, Route, useParams } from 'react-router-dom'

const CustomRedirect = (props: any) => {
    const params = useParams()
    useEffect(() => {
        props.effect && props.effect(params)
    }, [])

    return (
        <Redirect
            to={{
                pathname: props.to,
            }}
        />
    )
}

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const RedirectRouteWithEffect = ({ ...rest }) => {
    return <Route {...rest} render={(props) => <CustomRedirect {...props} effect={rest.effect} />} />
}

export default React.memo(RedirectRouteWithEffect)
