import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { getAccessToken } from 'src/utils/localstorage'

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const RestrictedRoute = ({ component: Component, ...rest }) => {
    const isAuthenticated = !!getAccessToken()
    return (
        <Route
            {...rest}
            render={(props) =>
                isAuthenticated ? (
                    <Redirect
                        to={{
                            pathname: '/',
                        }}
                    />
                ) : (
                    <Component {...props} />
                )
            }
        />
    )
}

export default React.memo(RestrictedRoute)
