import React from 'react'
import { Switch, Router } from 'react-router-dom'

import history from 'src/utils/history'
import routes, { IRouter } from 'src/routes'
import RedirectRoute from './RedirectRoute'
import AuthenticatedRoute from './AuthenticateRoute'
import RestrictedRoute from './RestrictedRoute'
import ErrorRoute from './ErrorRoute'
import RedirectRouteWithEffect from './RedirectRouteWithEffect'

const AppRoutes = () => {
    return (
        <Router history={history}>
            <Switch>
                {routes.map((route: IRouter, index) => {
                    if (route.redirect) {
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        return <RedirectRoute key={index} {...route} />
                    }

                    if (route.redirectWithEffect) {
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        return <RedirectRouteWithEffect key={index} {...route} />
                    }

                    if (route.requiredAuth) {
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        return <AuthenticatedRoute key={index} {...route} />
                    }

                    if (route.restricted) {
                        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                        // @ts-ignore
                        return <RestrictedRoute key={index} {...route} />
                    }

                    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                    // @ts-ignore
                    return <ErrorRoute key={index} {...route} />
                })}
            </Switch>
        </Router>
    )
}

export default AppRoutes
