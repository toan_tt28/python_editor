import React from 'react'
import { Result, Button } from 'antd'
import { Link } from 'react-router-dom'

const Forbidden: React.FC = () => {
    return (
        <Result
            status={403}
            title={403}
            subTitle="You do not have permission to access the page."
            extra={
                <Link to="/">
                    <Button type="primary">Back to home</Button>
                </Link>
            }
        />
    )
}

export default Forbidden
