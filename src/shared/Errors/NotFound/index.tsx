import React from 'react'
import { Result, Button } from 'antd'
import { Link } from 'react-router-dom'

const NotFound: React.FC = () => {
    return (
        <Result
            status={404}
            title={404}
            subTitle="The page you were looking for was not found."
            extra={
                <Link to="/">
                    <Button type="primary">Back to home</Button>
                </Link>
            }
        />
    )
}

export default NotFound
