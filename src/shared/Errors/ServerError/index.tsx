import React from 'react'
import { Result, Button } from 'antd'
import { Link } from 'react-router-dom'

const ServerError = () => {
    return (
        <Result
            status={500}
            title={500}
            subTitle="The page you are looking for cannot be displayed."
            extra={
                <Link to="/">
                    <Button type="primary">Back to home</Button>
                </Link>
            }
        />
    )
}

export default ServerError
