import React from 'react'
import styled from 'styled-components'
import { Spin } from 'antd'

const LoadingPage = () => {
    return (
        <Wrapper>
            <Spinner />
        </Wrapper>
    )
}

export default LoadingPage

const Wrapper = styled('div')`
  align-content: center;
  display: flex;
  flex: 1;
  flex-direction: column;
  justify-content: center;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 999;
  opacity: 0.5;
  background-color: black;
  transition: all 1s ease-in-out;
}
`

const Spinner = styled(Spin)`
    display: initial;
`
