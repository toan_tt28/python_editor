import React, { useCallback, useEffect } from 'react'
import { notification } from 'antd'
import { observer } from 'mobx-react'
import useRootStore from 'src/store/useRootStore'

export type NotiType = 'success' | 'info' | 'error' | 'warning'

type Props = {
    handleOffNotification: () => void
    isNotice: boolean
    type?: NotiType
    message?: string
    description?: string
    style?: React.CSSProperties
}

const key = 'random'
const NotificationComponent = (props: Props) => {
    const { isNotice, type = 'open', description, message, style, handleOffNotification } = props

    useEffect(() => {
        if (isNotice) {
            notification.close(key)
            notification[type]({
                message,
                description,
                style,
                key,
            })

            handleOffNotification()
        }
    })

    return <React.Fragment />
}

const Container = () => {
    const { globalStore } = useRootStore()

    const handleOffNotification = useCallback(() => {
        globalStore.offNotification()
    }, [])

    const containerProps = {
        handleOffNotification,
        ...globalStore.notification,
    }

    return <NotificationComponent {...containerProps} />
}

export default observer(Container)
