import React, { createContext, ReactElement } from 'react'
import { Observer } from 'mobx-react'

import * as store from 'src/store'

interface ChildrenProps<T> {
    children: (value: T) => ReactElement
}

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export const RootContext = createContext<IStore>(null)

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types,@typescript-eslint/ban-ts-comment
// @ts-ignore
export const RootConsumer = ({ children }: ChildrenProps<IStore>) => <Observer>{() => children(store)}</Observer>

const Provider: React.FC = ({ children }) => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    return <RootContext.Provider value={{ ...store }}>{children}</RootContext.Provider>
}

export default Provider
