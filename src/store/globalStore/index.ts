import { action, makeAutoObservable } from 'mobx'
import { NotiType } from 'src/shared/Notification'
import { getAccessToken, removeAccessToken, setAccessToken } from 'src/utils/localstorage'

type NotificationState = {
    isNotice: boolean
    type?: NotiType
    message?: string
    description?: string
}

const initialNotification = {
    isNotice: false,
}

type OnNotificationPayload = {
    type?: NotiType
    message: string
    description?: string
    style?: React.CSSProperties
}

export class GlobalStore {
    constructor() {
        makeAutoObservable(this)
    }

    isAuthenticated = !!getAccessToken()
    notification: NotificationState = initialNotification
    forceReload = 0

    successNotification = (payload: OnNotificationPayload) => {
        this.onNotification({ ...payload, type: 'success' })
    }

    errorNotification = (payload: OnNotificationPayload) => {
        this.onNotification({ ...payload, type: 'error' })
    }

    warningNotification = (payload: OnNotificationPayload) => {
        this.onNotification({ ...payload, type: 'warning' })
    }

    infoNotification = (payload: OnNotificationPayload) => {
        this.onNotification({ ...payload, type: 'info' })
    }

    @action
    onNotification = (payload: OnNotificationPayload) => {
        this.notification = { isNotice: true, ...payload }
    }

    @action
    offNotification = () => {
        this.notification = { isNotice: false }
    }

    @action
    handleForceReload = () => {
        this.forceReload = this.forceReload + 1
    }

    @action
    loginSuccess = (token: string) => {
        setAccessToken(token)
        this.isAuthenticated = true
    }

    @action
    logout = () => {
        removeAccessToken()
        this.isAuthenticated = false
    }
}

export default new GlobalStore()
