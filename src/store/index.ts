import { RouterStore } from '@superwf/mobx-react-router'
export const routerStore = new RouterStore()

export { default as globalStore } from './globalStore'
