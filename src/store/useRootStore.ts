import { useContext } from 'react'

import { RootContext } from 'src/shared/Provider'

export default function useRootStore() {
    return useContext(RootContext)
}
