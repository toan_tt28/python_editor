const color = {
    primary: '#9757EF',
    primaryDark: '#6F33C2',
    primaryLight: '#B681FF',

    secondary: '#37A0FF',
    secondaryDark: '#005FB5',
    secondaryLight: '#53ADFF',

    info: '#1774E1',
    infoDark: '#0D6AD6',
    infoLight: '#2685F5',

    success: '#50D38D',
    successDark: '#3EBE7A',
    successLight: '#62EAA1',

    error: '#F24D4D',
    errorDark: '#EB4444',
    errorLight: '#FD5E5E',

    warning: '#e94f4f',
    gray: '#828282',

    dark: '#191c1f',
    blue: 'blue',
    gray_500: '#828282',
    gray_2: '#EEEEEE',
    gray_200: '#F8F8F8',
}

export default color
