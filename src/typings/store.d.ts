import { RouterStore } from 'mobx-react-router'

declare global {
    /**
     * type for all store
     *
     * @interface IStore
     */
    interface IStore {
        routerStore: RouterStore
        globalStore: IGlobalStore.GlobalStore
    }
}
