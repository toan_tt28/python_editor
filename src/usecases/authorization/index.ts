import ttuta from 'src/repositories/ttuta'
import { globalStore, routerStore } from 'src/store'

export const postLogin = async (val: any) => {
    try {
        const { data } = await ttuta.login(val)
        const token = data.data.token

        if (!token) {
            throw new Error('Invalid Password')
        }

        globalStore.loginSuccess(token)
    } catch (e) {
        console.error(e)
        globalStore.errorNotification({ message: e.message })
    }
}

export const logout = () => {
    globalStore.logout()
    routerStore.history.push('/login')
}
