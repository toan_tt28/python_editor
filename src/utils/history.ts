import { syncHistoryWithStore } from '@superwf/mobx-react-router'
import { createBrowserHistory } from 'history'

import * as store from 'src/store'

const browserHistory = createBrowserHistory()
const history = syncHistoryWithStore(browserHistory, store.routerStore)

export default history
