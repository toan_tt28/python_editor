import React from 'react'
import Loadable from '@loadable/component'
import Loading from 'src/shared/Loading'

export const fallbackOptions = { fallback: <Loading /> }
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const loadComponent = (loader: () => Promise<any>) => Loadable(loader, fallbackOptions)
